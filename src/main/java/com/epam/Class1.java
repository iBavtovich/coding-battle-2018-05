package com.epam;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Class1 implements GameEngine {

    final static int[][] DIRECTIONS =  {{-1,0},{-1,-1},{0,-1},{1,-1},{1,0},{1,1},{0,1},{-1,1}};
    @Override
    public boolean[][] compute(boolean[][] initialState, int numberIterations) {
        List<Point> changedCells = new ArrayList<>();
        for (int k = 0; k < numberIterations; k++) {
            if (k == 0) {
                boolean[][] result = new boolean[initialState.length][initialState[0].length];
                for (int i = 0; i < initialState.length; i++) {
                    for (int j = 0; j < initialState[0].length; j++) {
                        result[i][j] = resolveNewState(initialState, i, j, changedCells);
                    }
                }
                initialState = result;
            }
            else {
                initialState = resolveForChangedAndNearState(initialState, changedCells);
            }
        }
        return initialState;
    }

    public  boolean resolveNewState(boolean[][] initialState, int coordY, int coordX, List<Point> changeStatus) {
        int numOfAliveCells = countAliveCells(initialState, coordX, coordY);
        if (initialState[coordY][coordX] == false) {
            if (numOfAliveCells == 3) {

                    changeStatus.add(new Point(coordX, coordY));
                                //change status
                return true;
            }
            return false;
        } else {
            if (numOfAliveCells < 2 || numOfAliveCells > 3) {

                    changeStatus.add(new Point(coordX, coordY));

                //change status
                return false;
            }
            return true;
        }
    }

    private  int countAliveCells(boolean[][] initialState, int coordX, int coordY) {
        int counterOfAlive = 0;
        int tmpCoordX;
        int tmpCoordY;
        int sizeX = initialState[0].length;
        int sizeY = initialState.length;
        for (int[] dir: DIRECTIONS) {
            tmpCoordX = coordX + dir[0];
            tmpCoordY = coordY + dir[1];
            if (tmpCoordX == -1 ){
                tmpCoordX = sizeX - 1;
            } else if (tmpCoordX == sizeX) {
                tmpCoordX = 0;
            }
            if (tmpCoordY == -1 ){
                tmpCoordY = sizeY - 1;
            } else if (tmpCoordY == sizeY) {
                tmpCoordY = 0;
            }
            if (initialState[tmpCoordY][tmpCoordX] == true) {
                counterOfAlive++;
            }
        }
        return counterOfAlive;
    }

    private boolean[][] resolveForChangedAndNearState(boolean[][] initialState, List<Point> changedCells) {
        List<Point> influencedPoints = new ArrayList<>();

        boolean[][] result = new boolean[initialState.length][initialState[0].length];
        for (int i = 0; i < initialState.length; i++) {
            for (int j = 0; j < initialState[i].length; j++) {
                result [i][j] = initialState[i][j];
            }
        }

        int sizeX = initialState[0].length;
        int sizeY = initialState.length;

        for (Point point: changedCells) {
            influencedPoints.add(point);
            for (int[] direction: DIRECTIONS) {
                int tmpCoordX;
                int tmpCoordY;
                tmpCoordX = point.x + direction[0];
                tmpCoordY = point.y + direction[1];
                if (tmpCoordX == -1 ){
                    tmpCoordX = sizeX - 1;
                } else if (tmpCoordX == sizeX) {
                    tmpCoordX = 0;
                }
                if (tmpCoordY == -1 ){
                    tmpCoordY = sizeY-1;
                } else if (tmpCoordY == sizeY) {
                    tmpCoordY = 0;
                }
                Point curPoint = new Point(tmpCoordX, tmpCoordY);
                if (!influencedPoints.contains(curPoint)) {
                    influencedPoints.add(curPoint);
                }
            }
        }
        changedCells.clear();
        for (Point point: influencedPoints) {
            result[point.y][point.x] = resolveNewState(initialState, point.y, point.x, changedCells);
        }

        return result;
    }

    private class Point {
        int x;
        int y;

        Point(int x, int y ){
            this.x = x;
            this.y = y;
        }
    }


}
