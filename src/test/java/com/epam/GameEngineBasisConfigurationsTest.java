package com.epam;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GameEngineBasisConfigurationsTest {

    @Test
    void standardAliveSectorTest2() {
        boolean[][] initialState = {
            {false, false, false, false, false},
            {false, true,  false, false, false},
            {false, false, false, true, false},
            {false, false, true,  false, false},
            {false, false, false, false, false}
        };
        int numberIterations = 1;

        boolean[][] gliderState = {
                {false, false, false, false, false},
                {false, false, true, false, false},
                {false, false, false, true, false},
                {false, true, true,  true, false},
                {false, false, false, false, false}
        };

        boolean[][] gliderStateRes1 = {
                {false, false, false, false, false},
                {false, false, false, false, false},
                {false, true, false, true, false},
                {false, false, true,  true, false},
                {false, false, true, false, false}
        };
        boolean[][] gliderStateRes2 = {
                {false, false, false, false, false},
                {false, false, false, false, false},
                {false, false, false, true, false},
                {false, true, false,  true, false},
                {false, false, true, true, false}
        };
        boolean[][] gliderStateRes3 = {
                {false, false, false, false, false},
                {false, false, false, false, false},
                {false, false, true, false, false},
                {false, false, false,  true, true},
                {false, false, true, true, false}
        };
        boolean[][] gliderStateRes4 = {
                {false, false, false, false, false},
                {false, false, false, false, false},
                {false, false, false, true, false},
                {false, false, false,  false, true},
                {false, false, true, true, true}
        };
        boolean[][] gliderStateRes5 = {
                {false, false, false, true, false},
                {false, false, false, false, false},
                {false, false, false, false, false},
                {false, false, true,  false, true},
                {false, false, false, true, true}
        };



        Class1 class1 = new Class1();
        boolean[][] result = class1.compute(initialState, numberIterations);
        boolean[][] result2 = class1.compute(initialState, 2);
        boolean[][] glidres1 = class1.compute(gliderState,1);
        boolean[][] glidres2 = class1.compute(gliderState,2);
        boolean[][] glidres3 = class1.compute(gliderState,3);
        boolean[][] glidres4 = class1.compute(gliderState,4);
        boolean[][] glidres5 = class1.compute(gliderState,5);


        assertArrayEquals(gliderStateRes1,glidres1);
        assertArrayEquals(gliderStateRes2,glidres2);
        assertArrayEquals(gliderStateRes3,glidres3);
        assertArrayEquals(gliderStateRes4,glidres4);
        assertArrayEquals(gliderStateRes5,glidres5);
        boolean[][] expectedState = {
                {false, false, false, false, false},
                {false, false, false, false, false},
                {false, false, true,  false, false},
                {false, false, false, false, false},
                {false, false, false, false, false}
        };

        boolean[][] expectedState2 = {
                {false, false, false, false, false},
                {false, false, false, false, false},
                {false, false, false,  false, false},
                {false, false, false, false, false},
                {false, false, false, false, false}
        };

        assertArrayEquals(expectedState, result);
        assertArrayEquals(expectedState2, result2);
    }
}